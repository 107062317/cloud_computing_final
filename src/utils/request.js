// [Modified] eslint-disable-next-line import/no-extraneous-dependencies
import axios from 'axios';

const service = axios.create({
    baseURL: 'http://demo.ivanouo.one:1586',
    timeout: 5000,
});
// request interceptors
service.interceptors.request.use((config) => {
    // console.log('[Axios]: request header ', config.headers);
    console.log('[Axios]: request ', config);
    return config;
}, (error) => {
    console.log(error);
    return Promise.reject(error);
});

// response interceptors
service.interceptors.response.use((response) => {
    console.log('[Axios]: response ', response.data);
    //return response.data;
    return response;
}, (error) => {
    const { response = {} } = error;
    const { data = null } = response;
    console.log('[Axios:Error]: ', data); // for debug
    return Promise.reject(data || error);
});

export default async function api(url, data, method, headers = {}, params = {}) {
    // Set token before request sent
    let token = '';
    if (typeof window !== 'undefined') {
        token = window.localStorage.getItem('auth_token');
    }
    if (token) {
        headers.Authorization = `Bearer ${token}`;
        headers['Access-Control-Allow-Origin'] = '*';
        // console.log(headers);
    }
    const result = await service.request({
        method,
        url,
        data,
        params,
        headers,
        maxContentLength: 10000000,
        maxBodyLength: 10000000,
        maxRedirects: 10000000,
    });
    return result;
}