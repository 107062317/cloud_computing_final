import Vue from 'vue';
import Vuex from 'vuex';

import App from './App.vue';
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueRouter from "vue-router";
import VueResource from "vue-resource";
import Routes from './routes';
import store from './stores'
import VueAgile from 'vue-agile'

Vue.use(VueAgile);
Vue.use(BootstrapVue);
Vue.use(VueResource);
Vue.use(IconsPlugin);
Vue.use(VueRouter);
Vue.use(Vuex);

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.config.productionTip = false


const router = new VueRouter({
  routes: Routes,
  mode: "history",
});

new Vue({
  render: h => h(App),
  router: router,
  store: store,
}).$mount('#app')
