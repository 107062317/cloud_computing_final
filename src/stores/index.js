import Vue from 'vue';
import Vuex from 'vuex';
//import { state, mutations } from './mutations.js'
//import * as getters from './getters.js'
//import * as actions from './actions.js'
import counter from './modules/counter.js'
import user from './modules/user.js'
import app from './modules/app.js'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    counter,
    user,
    app
  },

  strict: true
})
