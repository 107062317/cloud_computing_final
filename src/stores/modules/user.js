import { login } from '@/api/session';
//import { getUser } from '@/api/user';
const user = {
    state: {
        userId: null,
        isLoggedIn: false,
    },
    getters: {
        UserId: (state) => state.userId,
        IsLoggedIn: (state) => state.isLoggedIn,
    },
    mutations: {
        //SET_LOGIN_STATE: (state, currentUser) => {
        SET_LOGIN_STATE: (state, id) => {
            state.userId = id;
            state.isLoggedIn = true;
        },
        SET_LOGOUT_STATE: (state) => {
            state.userId = null;
            state.isLoggedIn = false;
        },
    },
    actions: {
        login({ commit }, { email, password }) {
            const loginProcess = async() => {
                const response = await login(email, password);
                const loginInfo = response.data;
                window.localStorage.setItem('auth_token', loginInfo.token);
                commit('SET_LOGIN_STATE', loginInfo.userId);
                console.log('[Vuex:login]: Login successfully');
                return response;
            };
            return loginProcess();
        },
        logout({ commit }) {
            const logoutProcess = async() => {
                window.localStorage.removeItem('auth_token');
                commit('SET_LOGOUT_STATE');
                console.log('[Vuex:logout]: Logout successfully');
            };
            return logoutProcess();
        },
    },
};

export default user;