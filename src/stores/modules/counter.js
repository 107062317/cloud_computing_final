const types = {
    INCREASE: 'count/INCREASE',
    DECREASE: 'count/DECREASE',
    COUNT_RESET: 'count/COUNT_RESET'
}

// count state 必須是 Object
const state = {
    count: 0
}

// getters 也可以整理到這邊直接返回 count 內容
const getters = {
    getCount: state => state.count
}

// actions 也是以 Object 形式建構。
const actions = {
actionIncrease ({ commit }) {
    commit(types.INCREASE);
},
actionDecrease ({ commit }) {
    commit(types.DECREASE);
},
actionCountReset ({ commit }) {
    commit(types.COUNT_RESET);
}
}

// mutations
const mutations = {
[types.INCREASE] (state) {
    state.count += 1;
    console.log('newINCREASE', 1, 'state?', state.count);
},
[types.DECREASE] (state) {
    state.count -= 1;
    console.log('newDECREASE', 1, 'state?', state.count);
},
[types.COUNT_RESET] (state) {
    state.count = 0;
    console.log('newCOUNT_RESET - state?', state.count);
}
}

const counter =  {
    state,
    getters,
    actions,
    mutations
}

export default counter