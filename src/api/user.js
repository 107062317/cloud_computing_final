import api from '@/utils/request';

let Image;

export function getAllUser() {
    return api('/users', {}, 'GET');
}

export function getUserProfile(user_id) {
    let path = '/users/' + user_id;
    return api(path, {}, 'GET');
}

export function addUser(params) {
    return api('/users', params, 'POST');
}

export function modifyUser(params) {
    return api('/users', params, 'PUT');
}

export function removeUser(params) {
    let path = '/users/' + params.user_id;
    return api(path, params, 'DELETE');
}
export function getImage() { // post id
    return Image;
}
export function saveImage(image) { // post id
    Image = image;
    return Image;
}