import api from '@/utils/request';
import FormData from 'form-data'

let Image;

export function addPost(params) {
    return api('/posts', params, 'POST');
}

export function getPosts(params) {
    var temp = '/posts?limit=' + params.limit + '&skip=' + params.skip + '&filter=' + params.filter;
    return api(temp, params, 'GET');
}

export function getPostsByCurrentUser(user_id) {
    let path = '/posts/byUserID/' + user_id;
    return api(path, user_id, 'GET');
}

export function getPost(post_id) { // post id
    let path = '/posts/' + post_id;
    return api(path, post_id, 'GET');
}

export function modifyPost(post_id, params) {
    let path = '/posts/content/' + post_id;
    return api(path, params, 'PUT');
}

export function increaseLikeCount(post_id) {
    let path = '/posts/likes/' + post_id;
    return api(path, post_id, 'PUT');
}

export function removePost(post_id) {
    let path = '/posts/' + post_id;
    return api(path, post_id, 'DELETE');
}

export function increaseViewCount(post_id) {
    let path = '/posts/views/' + post_id;
    return api(path, post_id, 'PUT');
}

export function uploadImage(image) { // post id
    var bodyFormData = new FormData();
    bodyFormData.append('file', image);
    return api('/file/upload', bodyFormData, 'POST', { 'Content-Type': 'multipart/form-data' });
}
export function getImage() { // post id
    return Image;
}
export function saveImage(image) { // post id
    Image = image;
    return Image;
}