import api from '@/utils/request';

//  [Modified] eslint-disable-next-line import/prefer-default-export
// api(url, data, method = 'POST', headers = {}, params = {})
//const baseURL = "http://demo.ivanouo.one:1586/"

export function login(email, password) {
    return api('/session', {user_account: email, user_password: password}, 'POST');
}