import homePage from "./components/Home.vue";
import blogPost from "./components/blogPost.vue";
import addBlog from "./components/addBlog.vue";
import single from "./components/single.vue"
import Logout from "./components/auth/logout.vue";
import personalhomePage from "./components/auth/PersonalHome.vue";
import editprofile from "./components/auth/editprofile.vue";
import Login from "./components/auth/Login.vue";
import Register from "./components/auth/Register.vue";
import ForgotPassword from './components/auth/forgotPassword.vue';
import AdminPanel from './components/auth/admin.vue';
import notLogin from './components/notLogin';
import authorhomePage from "./components/auth/authorHome.vue";

export default [
    { path: "/", component: homePage },
    { path: "/personalhome", component: personalhomePage },
    { path: "/editprofile", component: editprofile },
    { path: "/blogs", component: blogPost },
    { path: "/addBlog", component: addBlog },
    { path: "/single/:id", component: single },
    { path: "/notLogin", component: notLogin },
    { path: "/author/:uid", component: authorhomePage },
    {
        path: "/logout",
        component: Logout,
    },
    {
        path: "/forgot-password",
        component: ForgotPassword,
    },
    {
        path: "/login",
        component: Login,
    },
    {
        path: "/register",
        component: Register,
    },
    {
        path: "/admin",
        component: AdminPanel,
    }
];